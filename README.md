# Drona - Mihai Eduard si Kuszko Alexandru 334AB

## Introducere

Proiectul consta in realizarea unei drone folosind 4 motoare brushless, fiecare fiind conectat la cate un controler ESC si frame-ul fiind printat 3D.

Scopul este de a sta in aer si de a elimina perturbatiile folosind un algortim PID.

Aceasta poate avea multe utilitati pe baza ce aceasta are implementata mai multe functionalitati.

## Descriere generală

Proiectul nostru constă în construirea unei drone de la zero, folosind patru motoare brushless și patru ESC-uri. Pentru a controla și stabiliza drone, am implementat un modul senzor cu 9 axe MPU9250 și un controler PID.

Motoarele brushless și ESC-urile permit dronei să obțină o putere și o stabilitate superioare în timpul zborului. Modulul senzor cu 9 axe MPU9250 este responsabil pentru colectarea datelor privind orientarea, accelerația și viteza dronei. Aceste informații sunt utilizate de către controlerul PID pentru a menține stabilitatea și controlul precis al dronei în timpul zborului.

PID-ul compară datele primite de la modulul senzor cu referința dorită și ajustează în mod continuu viteza și unghiul de înclinare ale motoarelor pentru a menține dronei în echilibru și a obține o stabilitate optimă. Prin implementarea PID-ului, dronea va putea să compenseze automat orice deviație de la poziția și orientarea dorite, asigurând un zbor precis și controlat.

## Hardware Design

![schemablock.png](./schemablock.png)

Componetele folosite sunt:
- Arduino UNO
- Modul Senzor cu 9 Axe MPU9250
- 4 x ESC de 30 A pentru Motoare Brushless cu BEC (cu conectori banană)
- 4 x motoare brushless S500 V2 2216-880KV (2xCW si 2xCCW)

Cele 4 ESC-uri sunt conectate la GND si la pinii 4,5,6,7.

Modul Senzor cu 9 Axe MPU9250 este conectat astfel: VCC la 5V, GND la GND, SCL la A5 si SDA la A4.

Caracteristici tehnice:
- Giroscop:
    1) Output digital cu range programabil de ±250, ±500, ±1000 și ±2000°/sec;
    2) ADC pe 16 biți integrat;
    3) Filtru trece jos programabil;
    4) Curent de operare: 8uA - 3.2mA, în funcție de modul de operare;
- Accelerometru:
    1) Output digital cu range programabil de ±2g, ±4g, ±8g și ±16g;
    2) ADC pe 16 biți integrat;
    3) Curent consumat: 8uA - 450uA, în funcție de modul de operare
    4) Întreruperi programabile;
    5) Poate ieși din modul low power dacă detectează mișcare;

- Curent total de operare: 3.5mA;
- Comunicație I2C la 400kHz sau SPI la 1MHz sau SPI la 20MHz pentru citire senzori;
- Tensiune de funcționare modul: 3V - 5V;
- Senzor de temperatură cu output digital;
- Digital Motion Processing.

## Software Design

Pentru dezvoltarea aplicatiei am folsoit urmaotearele biblioteci:
- Servo.h
- MPU9250_WE.h
- Wire.h

Codul implementeaza controlarea si stabilizarea unei drone folosind un controler PID si un filtru Kalman pentru a combina datele de la giroscop si datele de la accelerometru.

Sunt preluate date de la modululSenzor cu 9 Axe MPU9250: aceeleratie si rotatie

Functia “kalman_1d” primește starea curentă a sistemului (KalmanState), incertitudinea (KalmanUncertainty), intrarea sistemului (KalmanInput) și măsurarea curentă (KalmanMeasurement). Calculează și actualizează starea Kalman și incertitudinea. Ieșirile sunt stocate într-un vector numit Kalman1DOutput.

Functia “pid_equation” implementează ecuația unui controler PID. Primește eroarea curentă (Error), coeficienții P, I și D, eroarea anterioară (PrevError) și integrala anterioară (PrevIterm). Calculează termenii P, I și D și returnează ieșirea controlerului PID și valorile de eroare și integrală actualizate.

Functia “reset_pid” reseteaza valorile anterioare ale roarilor si integralelor pentru o noua iteratie.

Functia “setup” initializeaza sistemul:
- Inițializarea și configurarea conexiunii cu ESC-urile (Electronic Speed Controllers) utilizate pentru controlul motoarelor quadcopterului.
- Inițializarea comunicării seriale pentru comunicarea cu computerul.
- Inițializarea și configurarea senzorului MPU9250.
- Calibrarea automată a senzorului MPU9250 pentru a determina offset-urile necesare pentru o măsurare corectă.
- Configurarea setărilor pentru filtrarea și scalarea valorilor de la giroscop și accelerometru.

Functia “loop” realizeaza in bucla continua controlul dronei:
- Controlul treptat al motoarelor quadcopterului prin modificarea valorilor de intrare pentru ESC-uri (val1, val2, val3, val4).
- Citirea datelor de la senzorul MPU9250, cum ar fi accelerația (AccX, AccY, AccZ), viteza unghiulară (RateRoll, RatePitch, RateYaw) și unghiurile de înclinare (AngleRoll, AnglePitch).
- Calculul unghiurilor de înclinare filtrate utilizând algoritmul Kalman (KalmanAngleRoll, KalmanAnglePitch).
- Calculul erorilor unghiurilor de înclinare și aplicarea controlerului PID pentru a obține ratele de înclinare dorite (DesiredRateRoll, DesiredRatePitch).
- Calculul erorilor de rate de înclinare și aplicarea controlerului PID pentru a obține valorile de intrare finale pentru motoare (InputRoll, InputPitch, InputYaw).
- Limitarea valorilor de intrare pentru a se potrivi cu limitele motoarelor și apelarea funcțiilor ESC pentru a seta noile valori pentru motoare (esc1.writeMicroseconds, esc2.writeMicroseconds, esc3.writeMicroseconds, esc4.writeMicroseconds).

## Rezultate Obţinute

Am reusit sa controlam fiecare motor individual in urma prelucarii datelor primite de la girscop si accelerometru.

Am reusit sa ridicam drona de la sol, dar stabilizarea ei nu a fost realizata in practica, probabil deoarece frecventa microcontroller-ului atmega328p, ce ruleaza la o freceventa de 16MHz nu reuseste sa proceseze suficient de repede datele.

In practica se vede cum datele giroscopului se modifica la modificarea pozitiei dronei si cum valorile de input ale ficarui motor sunt modificate individual incercandu-se stabilizarea sistemului. Calibrarea giroscopului nu este facuta cum trebuie si trebuiesc effectuate multe teste.

Chiar daca trimitem aceleasi valori pentru a comanda motoarele, acestea nu se invart cu aceeasi viteza si nu pornesc toate in acelasi timp, fiind anumite delay-uri.

![drona1.jpg](./drona1.jpg)

![drona2.jpg](./drona2.jpg)

![drona3.jpg](./drona3.jpg)

![droan4.mp4](./droan4.mp4)

![drona5.mp4](./drona5.mp4)
