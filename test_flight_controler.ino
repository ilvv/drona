 // ESCs controls
  #include <Servo.h>
  Servo esc1;
  Servo esc2;
  Servo esc3;
  Servo esc4;
  float val1 = 0;
  float val2 = 0;
  float val3 = 0;
  float val4 = 0;

  // Gyro-Acc controls
  #include <MPU9250_WE.h>
  #include <Wire.h>
  #define MPU9250_ADDR 0x68
  MPU9250_WE myMPU9250 = MPU9250_WE(MPU9250_ADDR);

  uint32_t LoopTimer;

  //PID
  float RatePitch, RateRoll, RateYaw;
  float ReceiverValue[]={0, 0, 0, 0, 0, 0, 0, 0};
  int ChannelNumber=0; 
  float DesiredRateRoll, DesiredRatePitch, DesiredRateYaw; // =0
  float ErrorRateRoll, ErrorRatePitch, ErrorRateYaw; // 0-rates
  float InputRoll, InputThrottle, InputPitch, InputYaw; //
  float PrevErrorRateRoll, PrevErrorRatePitch, PrevErrorRateYaw;
  float PrevItermRateRoll, PrevItermRatePitch, PrevItermRateYaw;
  float PIDReturn[]={0, 0, 0};
  float PRateRoll=0.6 ; float PRatePitch=PRateRoll; float PRateYaw=2;
  float IRateRoll=3.5 ; float IRatePitch=IRateRoll; float IRateYaw=12;
  float DRateRoll=0.03 ; float DRatePitch=DRateRoll; float DRateYaw=0;

  float AccX, AccY, AccZ;
  float AngleRoll, AnglePitch;
  float KalmanAngleRoll=0, KalmanUncertaintyAngleRoll=2*2;
  float KalmanAnglePitch=0, KalmanUncertaintyAnglePitch=2*2;
  float Kalman1DOutput[]={0,0};
  float DesiredAngleRoll, DesiredAnglePitch;
  float ErrorAngleRoll, ErrorAnglePitch;
  float PrevErrorAngleRoll, PrevErrorAnglePitch;
  float PrevItermAngleRoll, PrevItermAnglePitch;
  float PAngleRoll=2; float PAnglePitch=PAngleRoll;
  float IAngleRoll=0; float IAnglePitch=IAngleRoll;
  float DAngleRoll=0; float DAnglePitch=DAngleRoll;

  void kalman_1d(float KalmanState, float KalmanUncertainty, float KalmanInput, float KalmanMeasurement) {
    KalmanState=KalmanState+0.004*KalmanInput;
    KalmanUncertainty=KalmanUncertainty + 0.004 * 0.004 * 4 * 4;
    float KalmanGain=KalmanUncertainty * 1/(1*KalmanUncertainty + 3 * 3);
    KalmanState=KalmanState+KalmanGain * (KalmanMeasurement-KalmanState);
    KalmanUncertainty=(1-KalmanGain) * KalmanUncertainty;
    Kalman1DOutput[0]=KalmanState; 
    Kalman1DOutput[1]=KalmanUncertainty;
  }

  void pid_equation(float Error, float P , float I, float D, float PrevError, float PrevIterm) {
    float Pterm=P*Error;
    float Iterm=PrevIterm+I*(Error+PrevError)*0.004/2;
    if (Iterm > 400) Iterm=400;
    else if (Iterm <-400) Iterm=-400;
    float Dterm=D*(Error-PrevError)/0.004;
    float PIDOutput= Pterm+Iterm+Dterm;
    if (PIDOutput>400) PIDOutput=400;
    else if (PIDOutput <-400) PIDOutput=-400;
    PIDReturn[0]=PIDOutput;
    PIDReturn[1]=Error;
    PIDReturn[2]=Iterm;
  }
  void reset_pid(void) {
    PrevErrorRateRoll=0; PrevErrorRatePitch=0; PrevErrorRateYaw=0;
    PrevItermRateRoll=0; PrevItermRatePitch=0; PrevItermRateYaw=0;
    PrevErrorAngleRoll=0; PrevErrorAnglePitch=0;    
    PrevItermAngleRoll=0; PrevItermAnglePitch=0;
  }

  void setup() {

    // init ESCSs: 
    esc1.attach(4);
    esc2.attach(5);
    esc3.attach(6);
    esc4.attach(7);
    esc1.writeMicroseconds(1000);
    esc2.writeMicroseconds(1000);
    esc3.writeMicroseconds(1000);
    esc4.writeMicroseconds(1000);

    //init Serial
    
    Serial.begin(115200);

    //init communication with gyro-acc
    Wire.setClock(400000);
    Wire.begin();


    if(!myMPU9250.init()){
      Serial.println("MPU9250 does not respond");
    }
    else{
      Serial.println("MPU9250 is connected");
    }

    Serial.println("Position you MPU9250 flat and don't move it - calibrating...");
    delay(1000);
    myMPU9250.autoOffsets();
    Serial.println("Done!");

    // proiectare FTJ
    myMPU9250.enableGyrDLPF();

    myMPU9250.setGyrDLPF(MPU9250_DLPF_4);

    myMPU9250.setSampleRateDivider(5);

    myMPU9250.setGyrRange(MPU9250_GYRO_RANGE_250);

    myMPU9250.setAccRange(MPU9250_ACC_RANGE_2G);

    myMPU9250.enableAccDLPF(true);

    myMPU9250.setAccDLPF(MPU9250_DLPF_6);

    myMPU9250.setMagOpMode(AK8963_CONT_MODE_100HZ);

    delay(200);

  }

  float i = 1000;
  void loop() {
    if (i < 1500)
    {
      i+=0.2;
      InputThrottle = i;
      // Serial.println(InputThrottle);
    }
    // Control ESCs
    // val1= 0;
    // val1 = map(val1, 0, 1023, 1000, 2000);
    // esc1.writeMicroseconds(val1);

    // val2= 0;
    // val2 = map(val2, 0, 1023, 1000, 2000);
    // esc2.writeMicroseconds(val2);

    // val3= 0;
    // val3 = map(val3, 0, 1023, 1000, 2000);
    // esc3.writeMicroseconds(val3);

    // val4= 0;
    // val4 = map(val4, 0, 1023, 1000, 2000);
    // esc4.writeMicroseconds(val4);

    // get data from gyro-acc
    xyzFloat gValue = myMPU9250.getGValues();
    xyzFloat gyr = myMPU9250.getGyrValues();
    xyzFloat magValue = myMPU9250.getMagValues();
    // float temp = myMPU9250.getTemperature();
    float resultantG = myMPU9250.getResultantG(gValue);

    // Serial.println("Acceleration in g (x,y,z):");
    // Serial.print(gValue.x);
    // Serial.print("   ");
    // Serial.print(gValue.y);
    // Serial.print("   ");
    // Serial.println(gValue.z);
    // Serial.print("Resultant g: ");
    // Serial.println(resultantG);

    // Serial.println("Gyroscope data in degrees/s: ");
    // Serial.print(gyr.x);
    // Serial.print("   ");
    // Serial.print(gyr.y);
    // Serial.print("   ");
    // Serial.println(gyr.z);
    // Serial.println("********************************************");

    RateRoll=(float)gyr.x;
    RatePitch=(float)gyr.y;
    RateYaw=(float)gyr.z;
    AccX=(float)gValue.x;
    AccY=(float)gValue.y;
    AccZ=(float)gValue.z;
    AngleRoll=atan(AccY/sqrt(AccX*AccX+AccZ*AccZ))*1/(3.142/180);
    AnglePitch=-atan(AccX/sqrt(AccY*AccY+AccZ*AccZ))*1/(3.142/180);
    
    Serial.println("AccX: ");
    Serial.println(AccX);
    Serial.println("AccY: ");
    Serial.println(AccY);
    Serial.println("AccZ: ");
    Serial.println(AccZ);
    Serial.println("AngleRoll: ");
    Serial.println(AngleRoll);
    Serial.println("AnglePitch: ");
    Serial.println(AnglePitch);

    // Serial.println("********************************************");

    kalman_1d(KalmanAngleRoll, KalmanUncertaintyAngleRoll, RateRoll, AngleRoll);
    KalmanAngleRoll=Kalman1DOutput[0]; KalmanUncertaintyAngleRoll=Kalman1DOutput[1];
    kalman_1d(KalmanAnglePitch, KalmanUncertaintyAnglePitch, RatePitch, AnglePitch);
    KalmanAnglePitch=Kalman1DOutput[0]; KalmanUncertaintyAnglePitch=Kalman1DOutput[1];
    
    // DesiredAngleRoll=0.10*(ReceiverValue[0]-1500);
    DesiredAngleRoll=0.10*(1500-1500);

    // DesiredAnglePitch=0.10*(ReceiverValue[1]-1500);
    DesiredAnglePitch=0.10*(1500-1500);

    // InputThrottle = 1500;

    // DesiredRateYaw=0.15*(ReceiverValue[3]-1500);
    DesiredRateYaw=0.15*(1500-1500);

    ErrorAngleRoll=DesiredAngleRoll-KalmanAngleRoll;
    ErrorAnglePitch=DesiredAnglePitch-KalmanAnglePitch;

    pid_equation(ErrorAngleRoll, PAngleRoll, IAngleRoll, DAngleRoll, PrevErrorAngleRoll, PrevItermAngleRoll);

    DesiredRateRoll=PIDReturn[0]; 
    PrevErrorAngleRoll=PIDReturn[1];
    PrevItermAngleRoll=PIDReturn[2];

    pid_equation(ErrorAnglePitch, PAnglePitch, IAnglePitch, DAnglePitch, PrevErrorAnglePitch, PrevItermAnglePitch);
    DesiredRatePitch=PIDReturn[0]; 
    PrevErrorAnglePitch=PIDReturn[1];
    PrevItermAnglePitch=PIDReturn[2];
    ErrorRateRoll=DesiredRateRoll-RateRoll;
    ErrorRatePitch=DesiredRatePitch-RatePitch;
    ErrorRateYaw=DesiredRateYaw-RateYaw;

    pid_equation(ErrorRateRoll, PRateRoll, IRateRoll, DRateRoll, PrevErrorRateRoll, PrevItermRateRoll);
        InputRoll=PIDReturn[0];
        PrevErrorRateRoll=PIDReturn[1]; 
        PrevItermRateRoll=PIDReturn[2];
    pid_equation(ErrorRatePitch, PRatePitch,IRatePitch, DRatePitch, PrevErrorRatePitch, PrevItermRatePitch);
        InputPitch=PIDReturn[0]; 
        PrevErrorRatePitch=PIDReturn[1]; 
        PrevItermRatePitch=PIDReturn[2];
    pid_equation(ErrorRateYaw, PRateYaw,IRateYaw, DRateYaw, PrevErrorRateYaw, PrevItermRateYaw);
        InputYaw=PIDReturn[0]; 
        PrevErrorRateYaw=PIDReturn[1]; 
        PrevItermRateYaw=PIDReturn[2];

    if (InputThrottle > 1800) InputThrottle = 1800;

    val1= 1.024*(InputThrottle-InputRoll-InputPitch-InputYaw);
    val2= 1.024*(InputThrottle-InputRoll+InputPitch+InputYaw);
    val3= 1.024*(InputThrottle+InputRoll+InputPitch-InputYaw);
    val4= 1.024*(InputThrottle+InputRoll-InputPitch+InputYaw);

    // Serial.print("Motor1.1: ");
    // Serial.println(val1);
    // Serial.print("Motor2.1: ");
    // Serial.println(val2);
    // Serial.print("Motor3.1: ");
    // Serial.println(val3);
    // Serial.print("Motor4.1: ");
    // Serial.println(val4);
    // Serial.println("\n**************\n");

    if (val1 > 2000)val1 = 2000;
    if (val2 > 2000)val2 = 2000; 
    if (val3 > 2000)val3 = 2000; 
    if (val4 > 2000)val4 = 2000;

    // Serial.print("Motor1.2: ");
    // Serial.println(val1);
    // Serial.print("Motor2.2: ");
    // Serial.println(val2);
    // Serial.print("Motor3.2: ");
    // Serial.println(val3);
    // Serial.print("Motor4.2: ");
    // Serial.println(val4);
    // Serial.println("\n**************\n");

    int ThrottleIdle=1180;

    if (val1 < ThrottleIdle) val1 = ThrottleIdle;
    if (val2 < ThrottleIdle) val2 = ThrottleIdle;
    if (val3 < ThrottleIdle) val3 = ThrottleIdle;
    if (val4 < ThrottleIdle) val4 = ThrottleIdle;
    int ThrottleCutOff=1000;

    // Serial.print("Motor1.3: ");
    // Serial.println(val1);
    // Serial.print("Motor2.3: ");
    // Serial.println(val2);
    // Serial.print("Motor3.3: ");
    // Serial.println(val3);
    // Serial.print("Motor4.3: ");
    // Serial.println(val4);
    // Serial.println("\n**************\n");
    
    if (InputThrottle<1050) {
      val1=ThrottleCutOff; 
      val2=ThrottleCutOff;
      val3=ThrottleCutOff; 
      val4=ThrottleCutOff;
      reset_pid();
    }

    esc1.writeMicroseconds(val1);
    esc2.writeMicroseconds(val2);
    esc3.writeMicroseconds(val3);
    esc4.writeMicroseconds(val4);

    Serial.print("Motor1.4: ");
    Serial.println(val1);
    Serial.print("Motor2.4: ");
    Serial.println(val2);
    Serial.print("Motor3.4: ");
    Serial.println(val3);
    Serial.print("Motor4.4: ");
    Serial.println(val4);
    Serial.println("\n**************\n");

    Serial.println("********************************************");

    while (micros() - LoopTimer < 40000);
    LoopTimer=micros();


  }
