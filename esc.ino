#include <Servo.h>

Servo esc1;
Servo esc2;
Servo esc3;
Servo esc4;
int val1 = 0;
int val2 = 0;
int val3 = 0;
int val4 = 0;

void setup() {
  // put your setup code here, to run once:
	esc1.attach(4);
	esc2.attach(5);
	esc3.attach(6);
	esc4.attach(7);
	esc1.writeMicroseconds(1000);
	esc2.writeMicroseconds(1000);
	esc3.writeMicroseconds(1000);
	esc4.writeMicroseconds(1000);
	Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  val1 =  0;
  val1 = map(val1, 0, 1023, 1000, 2000);
  // Serial.println(val);
	esc1.writeMicroseconds(val1);

  val2= 0;
  val2 = map(val2, 0, 1023, 1000, 2000);
  // Serial.println(val);
	esc2.writeMicroseconds(val2);

  val3= 0;
  val3 = map(val3, 0, 1023, 1000, 2000);
  // Serial.println(val);
	esc3.writeMicroseconds(val3);

  val4= 0;
  val4 = map(val4, 0, 1023, 1000, 2000);
  // Serial.println(val);
	esc4.writeMicroseconds(val4);
}
